import 'dart:js';

import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
    routes: {
      extractArgumentScreen.routeName: (context) => const extractArgumentScreen(),
    },
    home: HomeScreen(),
  );
  }
}

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home Screen')),
        body: Center(child: ElevatedButton(
          onPressed: (){
            Navigator.pushNamed(context, 
            extractArgumentScreen.routeName,arguments: screenArgument(
              'Extract Argument Screen', 
              'this message is extracted in the build method'));
          },
          child:  Text('Naigate to screen that extract arguments'),
          ),)
    );
  }
}

class screenArgument {
  final String title;
  final String message;

  screenArgument(this.title, this.message);
}

//รับค่าจาก screenArgument
class extractArgumentScreen extends StatelessWidget {
  const extractArgumentScreen({Key? key}) : super(key: key);
  static const routeName = '/extractArgument';
  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context)!.settings.arguments as screenArgument;
    return Scaffold(
      appBar: AppBar(
        title: Text(args.title),
      ),
      body: Center(
        child: Text(args.message),
      ),
    );
  }
}
